# paperq

`paperq` is a tool for managing a reading queue for academic
literature.

## Usage

Called without any arguments, `paperq` will open the next article in
the queue.  The article is subsequently removed from the queue.
Command-line arguments provide further options for interacting with
the queue.  By default, for all actions operating on a file in the
queue, the file at the head of the queue (position 1) will be operated
upon.  If the `-n` argument is passed with a numeric argument, you may
specify a given file by its numeric position in the queue (use the
`-l` option to list the entire queue in order to see position
numbers).  Thus, to open the fifth paper in the queue, you would call
`paperq -n 5`.  To remove the eighth paper, use `paperq -r -n 8`.

### Command-line arguments

* `-n NUM`    set the queue position number on which to operate (default 1)

#### Manipulate and interact with the queue

* `-v`: open a file for viewing (default)
* `-a FILES`: add FILES to the queue
* `-r`: remove a file from the queue
* `-p`: peek at a file: open it, but do not remove it from the queue
* `-t`: print a file
* `-z`: create a tarball of the articles in the queue

#### Display information about the queue and individual papers:

* `-l`: list all of the documents in the queue
* `-i`: display information for a file
* `-f`: display file locations rather than bibliographic information

#### Other

* `-h`: display help

## Configuration

A configuration file should be created at
`$XDG_CONFIG_HOME/paperq/paperq.conf` (or
`$HOME/.config/paperq/paperq.conf`).  Each line in the file should
consist of an option name and a value, separated by a space.  Each
option can be defined only once.

Available options:

* `bibtex-dir`:  a directory containing BibTeX files to be used for
  retrieving bibliographic data
* `bibtex-file`: a BibTeX file to be used for retrieving bibliographic
  data
* `wrap` (default 64): the number of characters at which to wrap the
  bibliographic information.  Note that this does not include the tab
  character that is placed in front of the text.  Thus, if you want to
  wrap at 72 characters, and your terminal displays tabs as eight
  characters, set wrap to 64.
* `max-authors` (default 3): the number of authors to list before
  truncating with "et al."
* `open-cmd` (default `xdg-open %s`): the command used to open
  documents.  Use `%s` as a placeholder for the file name.
* `bibcache-size` (default 1000): the number of buckets in the DBM
  store of bibliographic data (see below)

### Displaying bibliographic information

The display of bibliographic data requires one or more BibTeX files.
Document file names are assumed to correspond with the BibTeX keys
(i.e.  "Smith2012.pdf" -> "`@article{Smith2012,...`") in the BibTeX
file.  Inside the configuration file, either declare the `bibtex-dir`
or `bibtex-file` variable followed by the corresponding file/directory
name, i.e.:

    bibtex-dir /home/john/bibtex

or

    bibtex-file /home/john/bibtex/mybib.bib

Only one of the two variable needs to specified.  Only one file
defined by bibtex-file will be used, regardless of the number defined.
bibtex-dir takes precedence over bibtex-file.

If a bibtex file is available, listing the documents in the queue
(`-l`) will display their bibliographic data rather than their
filenames.

## Caching bibliographic info with zeptodb

By default, paperq regenerates the bibliographic information for a
file every time you request information on it.  Instead, if zeptodb
[1] is installed on your system, the bibliographic text will be
cached, which may speed up the listing of larger numbers of articles.

Set the `bibcache-size` option in the config file in order to set the
approximate maximum number of articles that you expect to have in the
cache (default 1000).  If the cache grows to be larger than this
number, performance might be a bit slower.  On the other hand, setting
this to be needlessly large may also result in slower performance
(either way, the effect will probably be minimal unless your queue is
extremely large, in which case you probably have larger problems than
a slightly slower paperq).  Note that the bibcache-size is only used
when the cache is first created.  You cannot change the value at a
later time.

Footnotes: 
[1]  http://zeptodb.invergo.net

