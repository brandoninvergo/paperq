/*
 * format_text.h ---
 *
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 *
 * Author: Brandon Invergo <brandon@invergo.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FORMAT_TEXT_H
#define FORMAT_TEXT_H

#include <config.h>

#include <stdlib.h>
#include <errno.h>

#if HAVE_LIBUNISTRING
#include <uniconv.h>
#include <unitypes.h>
#include <unistdio.h>
#include <unistr.h>
#include <uniwidth.h>
#include <unilbrk.h>
#endif

#include <btparse.h>

#define BUFSIZE 50000
#define AUTHBUFSIZE 50000

int append_text (uint8_t *, uint8_t *, int);
void debrace (uint8_t *, uint8_t *);
void double_hyphen_to_en_dash (uint8_t *, uint8_t *);
void replace_spec_char (uint8_t *, int, ucs4_t, size_t, size_t *);
void special_chars (uint8_t *, uint8_t *);
void format_authors (char *, uint8_t *, int);
void wrap (uint8_t *, uint8_t *, uint8_t *, int);

#endif


