/*
 * paperbib.h ---
 *
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 *
 * Author: Brandon Invergo <brandon@invergo.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAPERBIB_H
#define PAPERBIB_H

#include <config.h>

#include <stdlib.h>
#include "argp.h"
#include <string.h>

#if HAVE_LIBUNISTRING
#include <uniconv.h>
#include <unitypes.h>
#include <unistr.h>
#include <unistdio.h>
#endif

#include <btparse.h>

#include "format_text.h"

#define BUFSIZE 50000
#define AUTHBUFSIZE 50000

uint8_t BIB_FORMAT[128] =
  "{author} {year} \033[1m{title}\033[0m {journal} {volume}{number}{pages}";

#endif
