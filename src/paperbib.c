/*
 * paperbib.c --- Print bibliographic information for a BibTeX entry
 *
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 *
 * Author: Brandon Invergo <brandon@invergo.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "paperbib.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "paperbib -- format bibliographic information from a BibTeX file"
  "\v"
  "The BibTeX file is supplied via stdin.  The citation entry to format "
  "is specified by its citation key, KEY.  The formatted citation is then "
  "printed to stdout";

static char args_doc[] = "KEY";

static struct argp_option options[] = {
  {"max-authors", 'a', "NUM", 0, "The maximum number of authors to list before "
   "truncating with 'et al.' (0 for no limit; default 3)"},
  {"wrap", 'w', "NUM", 0, "Wrap the output at column NUM (default no wrap)"},
  {"indent", 'i', "STR", 0, "Prefix each line of the output with STR"},
  {0}
};

struct arguments
{
  char *args[1];
  int max_authors;
  int wrap;
  uint8_t *indent;
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'a':
      if (arg < 0)
        argp_usage (state);
      arguments->max_authors = strtol (arg, NULL, 10);
      break;
    case 'w':
      if (arg < 0)
        argp_usage (state);
      arguments->wrap = strtol (arg, NULL, 10);
      break;
    case 'i':
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->indent = u8_strconv_from_locale(arg);
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;
    case ARGP_KEY_END:
      if (state->arg_num == 0)
        argp_usage (state);
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };


struct document
{
  uint8_t *title;
  uint8_t *author;
  uint8_t *year;
  uint8_t *journal;
  uint8_t *volume;
  uint8_t *number;
  uint8_t *pages;
};


AST *
get_bibtex_entry (char *key, AST *bibtex_entry)
{
  /* Locate a specific bibtex entry by key */
  AST *bibtex_tree;
  ushort btoptions = 0;
  boolean status;
  char *entry_key;
  bibtex_tree = bt_parse_file (NULL, btoptions, &status);
  if (status == FALSE)
    {
      fprintf (stderr, "%s: Error while parsing BibTeX file\n",
               PACKAGE_NAME);
      return NULL;
    }
  if (bibtex_tree == NULL)
    {
      fprintf (stderr, "%s: BibTeX file empty\n; %s\n",
               PACKAGE_NAME, strerror (errno));
      return NULL;
    }
  while (bibtex_entry = bt_next_entry (bibtex_tree, bibtex_entry))
    {
      /* Visit each entry sequentially and check if it matches our
         desired key. */
      entry_key = bt_entry_key (bibtex_entry);
      if (!strcmp (key, entry_key))
        {
          break;
        }
    }
  return bibtex_entry;
}


struct document
parse_entry (AST *bibtex_entry, int max_authors)
{
  /* Parse a bibtex entry. */
  AST *field = NULL;
  char *field_name;
  char *value_text;
  uint8_t *u_value_text;
  uint8_t value_clean[BUFSIZE] = "";
  size_t value_len;
  char author_buffer[AUTHBUFSIZE];
  uint8_t *u_author_buffer;
  struct document doc;

  while (field = bt_next_field (bibtex_entry, field, &field_name))
    {
      /* Walk through each field in the entry */
      value_text = bt_get_text (field);
      if (value_text == NULL)
        continue;
      u_value_text = u8_strconv_from_locale(value_text);
      value_len = u8_strlen (u_value_text);
      if (!u8_strcmp (field_name, "title"))
        {
          /* Found a title.  Clean it up and copy it into or document
             struct */
          debrace (value_clean, value_text);
          value_len = u8_strlen (value_clean);
          doc.title = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.title, value_clean, value_len);
          doc.title[value_len] = '\0';
          value_clean[0] = '\0';
        }
      else if (!u8_strcmp (field_name, "author"))
        {
          /* Found the authors.  Make the author list pretty and store
             that in the struct */
          format_authors (value_text, author_buffer, max_authors);
          u_author_buffer = u8_strconv_from_locale (author_buffer);
          debrace (value_clean, u_author_buffer);
          special_chars (value_clean, u_author_buffer);
          value_len = u8_strlen (value_clean);
          doc.author = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.author, value_clean, value_len);
          doc.author[value_len] = '\0';
          value_clean[0] = '\0';
          free (u_author_buffer);
        }
      else if (!u8_strcmp (field_name, "journal"))
        {
          /* Found the journal.  Clean it up... */
          debrace (value_clean, value_text);
          value_len = u8_strlen (value_clean);
          doc.journal = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.journal, value_text, value_len);
          doc.journal[value_len] = '\0';
          value_clean[0] = '\0';
        }
      else if (!u8_strcmp (field_name, "pages"))
        {
          /* Found the page range.  Replace '--' with an en dash */
          double_hyphen_to_en_dash (value_clean, value_text);
          value_len = u8_strlen (value_clean);
          doc.pages = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.pages, value_text, value_len);
          doc.pages[value_len] = '\0';
          value_clean[0] = '\0';
        }
      else if (!u8_strcmp (field_name, "volume"))
        {
          /* Found the journal volume */
          doc.volume = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.volume, value_text, value_len);
          doc.volume[value_len] = '\0';
        }
      else if (!u8_strcmp (field_name, "number"))
        {
          /* Found the issue number */
          doc.number = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.number, value_text, value_len);
          doc.number[value_len] = '\0';
        }
      else if (!u8_strcmp (field_name, "year"))
        {
          /* Found the publication year */
          doc.year = (uint8_t *)malloc (value_len * sizeof (uint8_t) + 1);
          u8_strncpy (doc.year, value_text, value_len);
          doc.year[value_len] = '\0';
        }
      free (value_text);
      free (u_value_text);
    }
  bt_free_ast (field);
  return doc;
}


int
format_entry (struct document doc, uint8_t *indent, int column)
{
  /* Print the bibliographic information according to the format in
     BIB_FORMAT.  This consists of tags in braces (e.g. {author},
     {year}, etc.) */
  uint8_t buffer[BUFSIZE] = "";
  uint8_t final[BUFSIZE] = "";
  int pos = 0;
  uint8_t *tok;
  uint8_t *format;
  uint8_t *ptr;
  format = u8_strdup (BIB_FORMAT);
  tok = u8_strtok (format, "{}", &ptr);
  while (tok != NULL)
    {
      if (!u8_strcmp (tok, "author"))
        {
          if (doc.author != NULL)
            {
              pos = append_text (buffer, doc.author, pos);
            }
        }
      else if (!u8_strcmp (tok, "title"))
        {
          if (doc.title != NULL)
            {
              pos = append_text (buffer, doc.title, pos);
            }
        }
      else if (!u8_strcmp (tok, "year"))
        {
          if (doc.year != NULL)
            {
              pos = append_text (buffer, "(", pos);
              pos = append_text (buffer, doc.year, pos);
              pos = append_text (buffer, ")", pos);
            }
        }
      else if (!u8_strcmp (tok, "journal"))
        {
          if (doc.journal != NULL)
            {
              pos = append_text (buffer, doc.journal, pos);
            }
        }
      else if (!u8_strcmp (tok, "volume"))
        {
          if (doc.volume != NULL)
            {
              pos = append_text (buffer, doc.volume, pos);
            }
        }
      else if (!u8_strcmp (tok, "number"))
        {
          if (doc.number != NULL)
            {
              pos = append_text (buffer, "(", pos);
              pos = append_text (buffer, doc.number, pos);
              pos = append_text (buffer, ")", pos);
            }
        }
      else if (!u8_strcmp (tok, "pages"))
        {
          if (doc.pages != NULL)
            {
              pos = append_text (buffer, ":", pos);
              pos = append_text (buffer, doc.pages, pos);
            }
        }
      else
        {
          pos = append_text (buffer, tok, pos);
        }
      tok = u8_strtok(NULL, "{}", &ptr);
    }
  buffer[pos] = '\0';
  wrap (final, buffer, indent, column);
  printf ("%s\n", final);
  free (format);
  return 0;
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  char *key;
  char bibtex_loc[512];
  AST *bibtex_entry = NULL;
  struct document doc;

  arguments.max_authors = 3;
  arguments.wrap = 0;
  arguments.indent = "";
  argp_parse (&argp, argc, argv, 0, 0, &arguments);
  key = arguments.args[0];

  bt_initialize ();
  bibtex_entry = get_bibtex_entry (key, bibtex_entry);
  if (bibtex_entry)
    {
      doc = parse_entry (bibtex_entry, arguments.max_authors);
      format_entry (doc, arguments.indent, arguments.wrap);
    }
  bt_free_ast (bibtex_entry);
  bt_cleanup ();
  exit (EXIT_SUCCESS);
}
