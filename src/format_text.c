/*
 * format_text.c ---
 *
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 *
 * Author: Brandon Invergo <brandon@invergo.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "format_text.h"
#include "spec_chars.c"


int
append_text (uint8_t *buffer, uint8_t *text, int pos)
{
  /* A helper function to safely append one string to another */
  size_t text_len;
  if (text == NULL)
    {
      return pos;
    }
  if (pos >= BUFSIZE)
    {
      return pos;
    }
  if (pos < 0)
    {
      return 0;
    }
  text_len = u8_strlen (text);
  if (text_len + pos >= BUFSIZE - 1)
    {
      text_len = BUFSIZE - pos - 1;
    }
  u8_strncpy (buffer + pos, text, text_len);
  return pos + text_len;
}


void
debrace (uint8_t *dest, uint8_t *source)
{
  /* Remove extraneous "quoting" braces found in your typical bibtex
     file */
  uint8_t *ptr = u8_strpbrk (source, "{}");
  size_t len = u8_strlen (source);
  while (ptr != NULL)
    {
      if ((size_t)(ptr - source) < len)
        {
          u8_move (ptr, ptr + 1, (len - (ptr - source)) * sizeof (uint8_t));
        }
      else if ((size_t)(ptr - source) == len)
        {
          source[len] = '\0';
        }
      len--;
      ptr = u8_strpbrk (source, "{}");
    }
  u8_strncpy (dest, source, len + 1);
  dest[len + 1] = '\0';
}


void
double_hyphen_to_en_dash (uint8_t *dest, uint8_t *source)
{
  /* Replace all double-hyphens ("--") in a string to en dashes
     (unicode 0x2013) */
  uint8_t *ptr = u8_strstr (source, "--");
  size_t len = u8_strlen (source);
  size_t pos = 0;
  while (ptr != NULL)
    {
      replace_spec_char (ptr, 2, 0x2013, ptr-source, &len);
      ptr = u8_strstr (source, "--");
    }
  u8_strncpy (dest, source, len + 1);
  dest[len + 1] = '\0';
}

void
replace_spec_char (uint8_t *ptr, int macro_len, ucs4_t new_char, size_t pos,
                   size_t *len)
{
  /* Replace a set of multiple characters with a single unicode
     character.  E.g. replace "--" with an en dash, or replace \'A
     with an accented 'a' */
  ucs4_t cur_char = NULL;
  uint8_t *move_to;
  const uint8_t *move_from;
  int i;

  /* The 2 probably shouldn't be hardcoded in the next line */
  if (u8_uctomb (ptr, new_char, 2) < 0)
    fprintf (stderr, "%s: Failed to write Unicode character to string\n; %s",
             PACKAGE_NAME, strerror(errno));
  move_from = u8_next (&cur_char, (const uint8_t *)ptr);
  if (move_from == NULL)
    return;
  move_to = (uint8_t *)move_from;
  for (i=1; i<macro_len; i++)
    {
      move_from = u8_next (&cur_char, move_from);
      if (move_from == NULL)
        return;
    }
  u8_move (move_to, move_from, (*len - pos - macro_len)*sizeof (uint8_t));
  *len -= (macro_len + 1) - u8_strmblen (ptr);
}


void
special_chars (uint8_t *dest, uint8_t *source)
{
  /* Find all escaped special characters (accented characters and
     such) and replace them with their unicode equivalent.  This uses
     the special character definitions in the spec_chars.gperf file
     (which generates spec_chars.c). */
  uint8_t macro[4] = "";
  const uint8_t *next_ptr;
  ucs4_t last_char, cur_char;
  size_t len = u8_strlen (source);
  size_t pos = 0;
  uint8_t *ptr = u8_strchr (source, '\\');
  struct spec_char *schar;
  size_t macro_len;

  while (ptr != NULL)
    {
      pos = u8_strcspn (source, "\\");
      next_ptr = u8_next (&cur_char, (const uint8_t *)ptr);
      if (next_ptr == NULL)
        break;
      next_ptr = u8_next (&cur_char, next_ptr);
      if (next_ptr == NULL)
        break;
      if (u8_strchr("OoiLl", cur_char))
        {
          last_char = cur_char;
          next_ptr = u8_next (&cur_char, next_ptr);
          if (next_ptr == NULL)
            break;
          if ((last_char == 'O' && cur_char == 'E') ||
              (last_char == 'o' && cur_char == 'e'))
            macro_len = 2;
          else
            macro_len = 1;
        }
      else
        macro_len = 2;
      u8_strncpy (macro, ptr, macro_len+1);
      macro[macro_len+1] = '\0';
      schar = spec_char_lookup ((char *)macro, (unsigned int)u8_strlen (macro));
      if (schar == NULL)
        {
          fprintf (stderr, "%s: Unknown special character macro %s\n",
                   PACKAGE_NAME, macro);
          break;
        }
      replace_spec_char (ptr, macro_len, schar->char_code, pos, &len);
      ptr = u8_strstr (source, "\\");
    }
  u8_strncpy (dest, source, len + 1);
  dest[len + 1] = '\0';
}


void
format_authors (char *author_text, uint8_t *author_buffer, int max_authors)
{
  /* Take a standard bibtex list of authors and format it nicely as a
     comma separated list.  Uses the btparse formatting functions for
     this purpose. */
  bt_name_format *name_format;
  bt_stringlist *author_list;
  bt_name *name;
  char *author_str;
  char *author_str_form;
  uint8_t *u_author_str_form;
  int i;
  size_t len;
  int pos = 0;

  name_format = bt_create_name_format ("vljf", TRUE);
  bt_set_format_text (name_format, BTN_FIRST, " ", ", ", NULL, "");
  bt_set_format_options (name_format, BTN_FIRST, TRUE, BTJ_NOTHING, BTJ_SPACE);

  author_list = bt_split_list (author_text, "and", NULL, 0, "name");
  for (i = 0; i < author_list->num_items; i++)
    {
      if (i != 0 && i == max_authors)
        {
          u8_strncpy (author_buffer + pos, "et al.", 6);
          pos += 6;
          break;
        }
      else
        {
          author_str = author_list->items[i];
          name = bt_split_name (author_str, NULL, 0, i+1);
          if (i == author_list->num_items - 1)
            {
              bt_set_format_text (name_format, BTN_FIRST, NULL, "",
                                  NULL, "");
              if (author_list->num_items > 1)
                bt_set_format_text (name_format, BTN_LAST, "and ", NULL,
                                    NULL, NULL);
            }
          author_str_form = bt_format_name (name, name_format);
          u_author_str_form = u8_strconv_from_locale(author_str_form);
          len = u8_strlen (u_author_str_form);
          if (len + pos >= AUTHBUFSIZE - 7)
            {
              u8_strncpy (author_buffer + pos, "et al.", 6);
              pos += 6;
            }
          else
            {
              u8_strncpy (author_buffer + pos, u_author_str_form, len);
              pos += len;
            }
          bt_free_name (name);
          free (author_str_form);
          free (u_author_str_form);
        }
    }
  bt_free_list (author_list);
  bt_free_name_format (name_format);
  author_buffer[pos] = '\0';
}


void
wrap (uint8_t *buffer, uint8_t *text, uint8_t *indent, int column)
{
  /* Wrap (fold) the given text to fit within a certain number of
     columns. */
  uint8_t *ptr;
  int pos = 0;
  const char *locale_encoding = locale_charset ();
  size_t len = u8_strlen (text);
  size_t remain = len;
  size_t indent_len = u8_strlen (indent);
  int line_width = 0;
  char *breaks;
  size_t i, line_start_i = 0;

  if (len > BUFSIZE || indent_len > BUFSIZE)
    {
      return;
    }
  if (column < 20 || u8_strwidth (text, locale_encoding) < column - indent_len)
    {
      u8_strncpy (buffer, indent, indent_len);
      pos += indent_len;
      u8_strncpy (buffer + pos, text, len);
      buffer[indent_len + len] = '\0';
      return;
    }
  breaks = (char *)malloc (len * sizeof (char));
  u8_width_linebreaks (text, len, column, indent_len, 0, NULL, locale_encoding,
                       breaks);
  ptr = buffer;
  for (i=1; i<remain; i++)
    {
      if (breaks[i] == UC_BREAK_POSSIBLE || breaks[i] == UC_BREAK_MANDATORY)
        {
          ptr = u8_stpncpy (ptr, indent, indent_len * sizeof (uint8_t));
          if (text[line_start_i + i - 1] == '\033' && i < remain - 5)
            i += 4;
          line_width = (size_t)i;
          ptr = u8_stpncpy (ptr, text+line_start_i,
                            line_width * sizeof (uint8_t));
          ptr = u8_stpncpy (ptr, "\n", sizeof (uint8_t));
          line_start_i += i;
          remain = len - line_start_i;
          breaks = (char *)realloc (breaks, remain * sizeof (char));
          u8_width_linebreaks (text+line_start_i, remain, column, indent_len, 0,
                               NULL, locale_encoding, breaks);
          i = 1;
        }
    }
  if (remain > 0)
    {
      ptr = u8_stpncpy (ptr, indent, indent_len);
      line_width = (size_t)i;
      u8_stpncpy (ptr, text+line_start_i, line_width*sizeof (uint8_t));
    }
  free (breaks);
  pos = (int)u8_strlen (buffer);
  buffer[pos] = '\0';
}
